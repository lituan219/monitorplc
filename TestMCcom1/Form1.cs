﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HslCommunication.Profinet.Melsec;
using HslCommunication;
using System.IO;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Drawing.Imaging;

namespace TestMCcom1
{
    public partial class Form1 : Form
    {
        int[] data = new int[1];
        private MelsecMcNet melsec_net = null;
        Timer timer = new Timer();
        Timer timer_heartbeat = new Timer();
        OperateResult<ushort[]> read = new OperateResult<ushort[]>();
        BackgroundWorker worker_trigger = new BackgroundWorker();
        bool mIsRunWorker = false;
        IniFile mIniFile;
        Stopwatch m_stopwatch = new Stopwatch();
        bool mIsStopWatchRunning = false;
        //Stopwatch m_stopwatch2 = new Stopwatch();
        //bool mIsStopWatchRunning2 = false;

        double mTacktime_valid = 0.0;
        double mTacktime_completed = 0.0;
        double mTacktime_result = 0.0;
        double mTacktime_executing = 0.0;
        public Form1()
        {

            InitializeComponent();
            InitIniFile();
            ReadConfigFromIniFile();
            timer.Interval = 100;
            timer.Tick += Timer_Tick;
            timer_heartbeat.Interval = 1000;
            timer_heartbeat.Tick += Timer_heartbeat_Tick;
            worker_trigger.WorkerSupportsCancellation = true;
            worker_trigger.DoWork += Worker_trigger_DoWork;
            chxSB09.CheckedChanged += ChxSB09_CheckedChanged;
            chxSB08.CheckedChanged += ChxSB08_CheckedChanged;
            chxCB24.CheckedChanged += ChxCB24_CheckedChanged;
            chxSB11.CheckedChanged += ChxSB11_CheckedChanged;
            txtOBValueFloat.TextChanged += TxtOBValueFloat_TextChanged;
        }

        private void TxtOBValueFloat_TextChanged(object sender, EventArgs e)
        {
            if (m_stopwatch.IsRunning)
            {
                //m_stopwatch.Stop();
                mTacktime_result = m_stopwatch.ElapsedMilliseconds;
                //lblTaktTime.Text = mTacktime_valid.ToString() + " ms";
                //mIsStopWatchRunning = false;
            }
        }

        private void ChxSB11_CheckedChanged(object sender, EventArgs e)
        {
            if (m_stopwatch.IsRunning && chxSB11.Checked)
            {
                //m_stopwatch.Stop();
                mTacktime_valid = m_stopwatch.ElapsedMilliseconds;
                lblTaktTime.Text = mTacktime_valid.ToString() + " ms";
                //mIsStopWatchRunning = false;
            }
        }

        private void ChxSB08_CheckedChanged(object sender, EventArgs e)
        {
            if (m_stopwatch.IsRunning && !chxSB08.Checked)
            {
                mTacktime_executing = m_stopwatch.ElapsedMilliseconds;
            }
        }

        private void ChxCB24_CheckedChanged(object sender, EventArgs e)
        {
            if (chxCB24.Checked)
            {
                m_stopwatch.Restart();
            }
        }

        private void ChxSB09_CheckedChanged(object sender, EventArgs e)
        {
            if (m_stopwatch.IsRunning)
            {
                //m_stopwatch2.Stop();
                mTacktime_completed = m_stopwatch.ElapsedMilliseconds;
                label7.Text = mTacktime_completed.ToString() + " ms";
            }
        }

        private void Timer_heartbeat_Tick(object sender, EventArgs e)
        {
        }

        private void InitIniFile()
        {
            if (File.Exists(Environment.CurrentDirectory + "\\Config.ini"))
            {
                mIniFile = new IniFile(Environment.CurrentDirectory + "\\Config.ini");
            }
            else
            {
                using (FileStream fs = File.Create(Environment.CurrentDirectory + "\\Config.ini"))
                {
                    mIniFile = new IniFile(Environment.CurrentDirectory + "\\Config.ini");
                }
                SaveConfigToIniFile(mIniFile, "192.168.10.39", "3005", "D8000", "D8100", "D8002", "D8010", "D8016", "D8102", "D8111", "D8117");
            }

        }

        private void SaveConfigToIniFile(IniFile ConfigIni, string addr, string port, string controlBlock, string statusBlock, string ibValue, string ibFloat, string ibBarcode, string obValue, string obFloat, string obTray)
        {
            ConfigIni.Write("PLCAddress", addr);
            ConfigIni.Write("PLCPort", port);
            ConfigIni.Write("ControlBlock", controlBlock);
            ConfigIni.Write("StatusBlock", statusBlock);
            ConfigIni.Write("InputBlockValue", ibValue);
            ConfigIni.Write("InputBlockFloat", ibFloat);
            ConfigIni.Write("InputBlockBarcode", ibBarcode);
            ConfigIni.Write("OutputBlockValue", obValue);
            ConfigIni.Write("OutputBlockFloat", obFloat);
            ConfigIni.Write("OutputBlockTray", obTray);
        }

        private void ReadConfigFromIniFile()
        {
            string addr = mIniFile.Read("PLCAddress");
            string port = mIniFile.Read("PLCPort");
            string controlBlock = mIniFile.Read("ControlBlock");
            string statusBlock = mIniFile.Read("StatusBlock");
            string ibValue = mIniFile.Read("InputBlockValue");
            string ibFloat = mIniFile.Read("InputBlockFloat");
            string ibBarcode = mIniFile.Read("InputBlockBarcode");
            string obValue = mIniFile.Read("OutputBlockValue");
            string obFloat = mIniFile.Read("OutputBlockFloat");
            string obTray = mIniFile.Read("OutputBlockTray");
            txtIPAddress.Text = addr;
            txtPort.Text = port;
            txtCBAddress.Text = controlBlock;
            txtSBAddress.Text = statusBlock;
            txtIBAddress.Text = ibValue;
            txtIBAddressFloat.Text = ibFloat;
            txtIBAddressBarcode.Text = ibBarcode;
            txtOBAddress.Text = obValue;
            txtOBAddressFloat.Text = obFloat;
            txtOBAddressTray.Text = obTray;
        }

        bool isWriteHeader = false;
        private void Worker_trigger_DoWork(object sender, DoWorkEventArgs e)
        {
            while (mIsRunWorker)
            {

                if (!File.Exists(@"E:\TactTimeSLMP.csv"))
                {
                    var file = File.Create(@"E:\TactTimeSLMP.csv");
                    isWriteHeader = true;
                    file.Close();
                }

                using (StreamWriter sw = new StreamWriter(@"E:\TactTimeSLMP.csv", true))
                {
                    if (isWriteHeader == true)
                    {
                        sw.WriteLine("Date,Result Valid,Inspection Completed,Result Updated,Inspection Executing");
                        isWriteHeader = false;
                    }
                    sw.WriteLine(string.Format(DateTime.Now.ToString("ddMMyyyy HH:mm:ss.fff") + ",{0},{1},{2},{3}", mTacktime_valid.ToString(), mTacktime_completed.ToString(), mTacktime_result.ToString(), mTacktime_executing.ToString()));
                    sw.Flush();
                    sw.Close();
                }
                m_stopwatch.Stop();
                melsec_net.Write(txtWrAddress0.Text, 1);
                System.Threading.Thread.Sleep(500);
                melsec_net.Write(txtWrAddress0.Text, 256);
                System.Threading.Thread.Sleep(Int32.Parse(txtCycleTime.Text.Trim()));
                melsec_net.Write(txtWrAddress.Text, 8);
                System.Threading.Thread.Sleep(200);
                melsec_net.Write(txtWrAddress.Text, 0);
                if (worker_trigger.CancellationPending)
                {
                    e.Cancel = true;
                }
            }
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            try
            {
                txtCBValue.Text = ReadBlock(txtCBAddress.Text, 2, true);
                ChangeStatus(txtCBValue.Text, 4, chxCB04);
                ChangeStatus(txtCBValue.Text, 7, chxCB07);
                ChangeStatus(txtCBValue.Text, 16, chxCB16);
                ChangeStatus(txtCBValue.Text, 24, chxCB24);

                txtSBValue.Text = ReadBlock(txtSBAddress.Text, 2, true);
                ChangeStatus(txtSBValue.Text, 7, chxSB07);
                ChangeStatus(txtSBValue.Text, 8, chxSB08);
                ChangeStatus(txtSBValue.Text, 8, checkBox2);
                ChangeStatus(txtSBValue.Text, 9, chxSB09);
                ChangeStatus(txtSBValue.Text, 9, checkBox1);
                ChangeStatus(txtSBValue.Text, 11, chxSB11);
                ChangeStatus(txtSBValue.Text, 12, chxSB12);
                ChangeStatus(txtSBValue.Text, 13, chxSB13);
                ChangeStatus(txtSBValue.Text, 14, chxSB14);
                ChangeStatus(txtSBValue.Text, 16, chxSB16);
                ChangeStatus(txtSBValue.Text, 24, chxSB24);

                txtIBValue.Text = ReadBlock(txtIBAddress.Text, 8, false);
                txtIBValueFloat.Text = ReadBlock(txtIBAddressFloat.Text, 3);
                txtIBValueBarcode.Text = ReadStringBlock(txtIBAddressBarcode.Text, 40);
                txtOBValue.Text = ReadBlock(txtOBAddress.Text, 9, false);
                txtOBValueFloat.Text = ReadBlock(txtOBAddressFloat.Text, 50);
                txtOBValueTrayBit.Text = ReadBlock(txtOBAddressTray.Text, 6, true);
            }
            catch (Exception ex)
            {
                melsec_net.ConnectClose();
                lblStatus.Text = "Disconnected!";
            }
        }

        private void ChangeStatus(string text, int v, CheckBox chx)
        {

            bool return_bool = true;
            string bit = "0";
            string[] split = text.Trim().Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            if (split.Count() > 0)
            {
                if (v < 16)
                {
                    bit = split[0].Substring(15 - v, 1);
                }
                else
                {
                    bit = split[1].Substring(31 - v, 1);
                }
                return_bool = bit == "1" ? true : false;
            }
            chx.Checked = return_bool;
            if (return_bool)
            {
                chx.BackColor = Color.Lime;
            }
            else
            {
                chx.BackColor = Color.OrangeRed;
            }
        }

        private string ReadBlock(string address, ushort numberblock, bool isConvertToBin)
        {
            OperateResult<ushort[]> read = new OperateResult<ushort[]>();
            string result = string.Empty;
            string bin = string.Empty;
            if (read != null)
            {
                read = melsec_net.ReadUInt16(address, numberblock);
                if (read.IsSuccess)
                {
                    for (int i = 0; i < read.Content.Length; ++i)
                    {
                        if (!isConvertToBin)
                        {
                            result += read.Content[i].ToString() + ",";
                        }
                        else
                        {
                            result += CompleteBinaryString(Convert.ToString((int)read.Content[i], 2)) + ",";
                        }
                    }

                    return result;
                }
                else
                {
                    return "Fail";
                }
            }
            else
            {
                return "null";
            }
        }

        private string CompleteBinaryString(string bin)
        {
            string outputString = "";
            int outputStringLength = 0;
            if (bin == "2")
            {
                bin = "10";
            }
            outputString = bin;
            outputStringLength = outputString.Length;
            if (outputStringLength < 16)
            {
                for (int i = 0; i < 16 - outputStringLength; i++)
                {
                    outputString = "0" + outputString;
                }
            }
            return outputString;
        }
        private string ReadBlock(string address, ushort numberblock)
        {
            OperateResult<float[]> read = new OperateResult<float[]>();
            string result = string.Empty;
            if (read != null)
            {
                read = melsec_net.ReadFloat(address, numberblock);
                if (read.IsSuccess)
                {
                    for (int i = 0; i < read.Content.Length; ++i)
                    {
                        result += (float)read.Content[i] + ",";
                    }

                    return result;
                }
                else
                {
                    return "Fail";
                }
            }
            else
            {
                return "null";
            }
        }

        private string ReadStringBlock(string address, ushort numberblock)
        {
            OperateResult<string> read = new OperateResult<string>();
            string result = string.Empty;
            if (read != null)
            {
                read = melsec_net.ReadString(address, numberblock);
                if (read.IsSuccess)
                {
                    for (int i = 0; i < read.Content.Length; ++i)
                    {
                        result += read.Content[i];
                    }
                    return result;
                }
                else
                {
                    return "Fail";
                }
            }
            else
            {
                return "null";
            }
        }
        private string ExtractWordFromString(string source, int wordIndex)
        {
            string output = "null";
            if (source != "")
            {
                string[] splited = source.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                if (wordIndex < splited.Length)
                {
                    output = splited[wordIndex];
                }
            }
            return output;
        }
        private bool ExtractBitFrom2Word(string source, int bitIndex)
        {
            string firstWord = "";
            string secondWord = "";
            bool bit = false;
            int num = 0;
            if (source != "")
            {
                string[] splited = source.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                firstWord = splited[0];
                secondWord = splited[1];
                if (splited[0].Length < 16)
                {
                    for (int i = 0; i < 16 - splited[0].Length; i++)
                    {
                        firstWord = "0" + firstWord;
                    }
                }
                if (splited[1].Length < 16)
                {
                    for (int i = 0; i < 16 - splited[1].Length; i++)
                    {
                        secondWord = "0" + secondWord;
                    }
                }
                if (bitIndex < 16)
                {
                    num = bitIndex;
                    bit = firstWord.Substring(15 - num, 1) == "1" ? true : false;
                }
                else
                {
                    num = 32 - bitIndex;
                    bit = secondWord.Substring(num - 1, 1) == "1" ? true : false;
                }
            }
            return bit;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            string ip = txtIPAddress.Text;
            int port = int.Parse(txtPort.Text);
            melsec_net = new MelsecMcNet(ip, port);
            OperateResult connectResult = melsec_net.ConnectServer();

            if (connectResult.IsSuccess)
            {
                lblStatus.Text = "Connected!";
                timer.Start();
                timer_heartbeat.Start();
            }
            else
            {
                lblStatus.Text = "Connect Fail!";
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            melsec_net.ConnectClose();
            timer.Stop();
            timer_heartbeat.Stop();
            lblStatus.Text = "Disconnected!";
        }

        private void btnWrite_Click(object sender, EventArgs e)
        {
            try
            {

            if (txtWrValue0.Text.Trim() != "")
                melsec_net.Write(txtWrAddress0.Text, Int16.Parse(txtWrValue0.Text));
            if (txtWrValue1.Text.Trim() != "")
            {
                if (txtWrValue1.Text.Trim().Contains(","))
                {
                    string[] split = txtWrValue1.Text.Trim().Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                    List<short> intList = new List<short>();
                    foreach(string s in split)
                    {
                        intList.Add(short.Parse(s));
                    }
                    melsec_net.Write(txtWrAddress1.Text, intList.ToArray());
                }
                else
                {
                    melsec_net.Write(txtWrAddress1.Text, Int16.Parse(txtWrValue1.Text));
                }
            }
            if (txtWrValue2.Text.Trim() != "")
            {
                if (txtWrValue2.Text.Trim().Contains(","))
                {
                    string[] split = txtWrValue2.Text.Trim().Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                    List<float> floatList = new List<float>();
                    foreach (string s in split)
                    {
                        floatList.Add(float.Parse(s));
                    }
                    melsec_net.Write(txtWrAddress2.Text, floatList.ToArray());
                }
                else
                {
                    melsec_net.Write(txtWrAddress2.Text, float.Parse(txtWrValue2.Text));
                }
            }
            if (txtWrValue3.Text.Trim() != "")
                melsec_net.Write(txtWrAddress3.Text, txtWrValue3.Text);

            }
            catch(Exception ex) { }
        }

        private void btnRead_Click(object sender, EventArgs e)
        {
            OperateResult<float[]> read = melsec_net.ReadFloat(txtReadAddress.Text, 1);
            if (read.IsSuccess)
            {
                string result = string.Empty;
                for (int i = 0; i < read.Content.Length; ++i)
                {
                    result += Convert.ToString(float.Parse(read.Content[i].ToString())) + ",";
                }

                txtValueRead.Text = result;
            }
            else
            {
                txtValueRead.Text = "Reading Fail";
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //string ip = txtIPAddress.Text;
            //int port = int.Parse(txtPort.Text);
            //melsec_net = new MelsecMcNet(ip, port);
            //OperateResult connectResult = melsec_net.ConnectServer();

            //if (connectResult.IsSuccess)
            //{
            //    textBox1.Text = "Connected!";
            //}
            //else
            //{
            //    textBox1.Text = "Connect Fail";
            //}
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (txtWrValue.Text.Trim() != "")
                melsec_net.Write(txtWrAddress.Text, Int16.Parse(txtWrValue.Text));

        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (!worker_trigger.IsBusy)
            {
                mIsRunWorker = true;
                worker_trigger.RunWorkerAsync();
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (worker_trigger.IsBusy)
            {
                mIsRunWorker = false;
                worker_trigger.CancelAsync();
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Are you sure to close program?", "Warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.Cancel)
            {
                e.Cancel = true;
            }
            else
            {
                SaveConfigToIniFile(mIniFile, txtIPAddress.Text, txtPort.Text, txtCBAddress.Text, txtSBAddress.Text, txtIBAddress.Text, txtIBAddressFloat.Text, txtIBAddressBarcode.Text, txtOBAddress.Text, txtOBAddressFloat.Text, txtOBAddressTray.Text);
                Shutdown();
            }
        }

        private void Shutdown()
        {
            if (worker_trigger.IsBusy)
            {
                worker_trigger.CancelAsync();
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            txtWrValue1.Text = "";
            txtWrValue2.Text = "";
            txtWrValue3.Text = "";
        }
    }
}

