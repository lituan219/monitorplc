﻿namespace TestMCcom1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.txtWrValue0 = new System.Windows.Forms.TextBox();
            this.txtWrAddress0 = new System.Windows.Forms.TextBox();
            this.btnWrite = new System.Windows.Forms.Button();
            this.btnRead = new System.Windows.Forms.Button();
            this.txtReadAddress = new System.Windows.Forms.TextBox();
            this.txtValueRead = new System.Windows.Forms.TextBox();
            this.chxCB04 = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.chxCB07 = new System.Windows.Forms.CheckBox();
            this.chxCB16 = new System.Windows.Forms.CheckBox();
            this.chxCB24 = new System.Windows.Forms.CheckBox();
            this.chxSB14 = new System.Windows.Forms.CheckBox();
            this.chxSB09 = new System.Windows.Forms.CheckBox();
            this.chxSB08 = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.chxSB07 = new System.Windows.Forms.CheckBox();
            this.chxSB24 = new System.Windows.Forms.CheckBox();
            this.chxSB16 = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtWrAddress1 = new System.Windows.Forms.TextBox();
            this.txtWrValue1 = new System.Windows.Forms.TextBox();
            this.txtWrAddress2 = new System.Windows.Forms.TextBox();
            this.txtWrValue2 = new System.Windows.Forms.TextBox();
            this.txtIBAddress = new System.Windows.Forms.TextBox();
            this.txtOBAddress = new System.Windows.Forms.TextBox();
            this.txtIBValue = new System.Windows.Forms.TextBox();
            this.txtOBValue = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.txtCBValue = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txtSBValue = new System.Windows.Forms.TextBox();
            this.txtCBAddress = new System.Windows.Forms.TextBox();
            this.txtSBAddress = new System.Windows.Forms.TextBox();
            this.txtWrAddress3 = new System.Windows.Forms.TextBox();
            this.txtWrValue3 = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.txtIPAddress = new System.Windows.Forms.TextBox();
            this.txtPort = new System.Windows.Forms.TextBox();
            this.txtIBValueFloat = new System.Windows.Forms.TextBox();
            this.txtOBValueFloat = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtOBValueTrayBit = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtIBValueBarcode = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.txtWrAddress = new System.Windows.Forms.TextBox();
            this.txtWrValue = new System.Windows.Forms.TextBox();
            this.txtIBAddressFloat = new System.Windows.Forms.TextBox();
            this.txtIBAddressBarcode = new System.Windows.Forms.TextBox();
            this.txtOBAddressFloat = new System.Windows.Forms.TextBox();
            this.txtOBAddressTray = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.lblStatus = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.chxSB13 = new System.Windows.Forms.CheckBox();
            this.chxSB12 = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.lblTaktTime = new System.Windows.Forms.Label();
            this.chxSB11 = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.button6 = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtCycleTime = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(10, 83);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(64, 25);
            this.button1.TabIndex = 0;
            this.button1.Text = "Connect";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(87, 83);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(77, 25);
            this.button2.TabIndex = 1;
            this.button2.Text = "Disconnect";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // txtWrValue0
            // 
            this.txtWrValue0.Location = new System.Drawing.Point(190, 181);
            this.txtWrValue0.Name = "txtWrValue0";
            this.txtWrValue0.Size = new System.Drawing.Size(77, 20);
            this.txtWrValue0.TabIndex = 3;
            this.txtWrValue0.Text = "256";
            // 
            // txtWrAddress0
            // 
            this.txtWrAddress0.Location = new System.Drawing.Point(67, 181);
            this.txtWrAddress0.Name = "txtWrAddress0";
            this.txtWrAddress0.Size = new System.Drawing.Size(77, 20);
            this.txtWrAddress0.TabIndex = 4;
            this.txtWrAddress0.Text = "D8001";
            // 
            // btnWrite
            // 
            this.btnWrite.Location = new System.Drawing.Point(279, 179);
            this.btnWrite.Name = "btnWrite";
            this.btnWrite.Size = new System.Drawing.Size(64, 25);
            this.btnWrite.TabIndex = 5;
            this.btnWrite.Text = "Write";
            this.btnWrite.UseVisualStyleBackColor = true;
            this.btnWrite.Click += new System.EventHandler(this.btnWrite_Click);
            // 
            // btnRead
            // 
            this.btnRead.Location = new System.Drawing.Point(279, 345);
            this.btnRead.Name = "btnRead";
            this.btnRead.Size = new System.Drawing.Size(64, 25);
            this.btnRead.TabIndex = 8;
            this.btnRead.Text = "Read";
            this.btnRead.UseVisualStyleBackColor = true;
            this.btnRead.Click += new System.EventHandler(this.btnRead_Click);
            // 
            // txtReadAddress
            // 
            this.txtReadAddress.Location = new System.Drawing.Point(67, 346);
            this.txtReadAddress.Name = "txtReadAddress";
            this.txtReadAddress.Size = new System.Drawing.Size(77, 20);
            this.txtReadAddress.TabIndex = 7;
            this.txtReadAddress.Text = "D8100";
            // 
            // txtValueRead
            // 
            this.txtValueRead.Location = new System.Drawing.Point(190, 346);
            this.txtValueRead.Name = "txtValueRead";
            this.txtValueRead.Size = new System.Drawing.Size(77, 20);
            this.txtValueRead.TabIndex = 6;
            // 
            // chxCB04
            // 
            this.chxCB04.AutoSize = true;
            this.chxCB04.Location = new System.Drawing.Point(21, 75);
            this.chxCB04.Name = "chxCB04";
            this.chxCB04.Size = new System.Drawing.Size(121, 17);
            this.chxCB04.TabIndex = 9;
            this.chxCB04.Text = "(04) Change Recipe";
            this.chxCB04.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Control Block";
            // 
            // chxCB07
            // 
            this.chxCB07.AutoSize = true;
            this.chxCB07.Location = new System.Drawing.Point(21, 103);
            this.chxCB07.Name = "chxCB07";
            this.chxCB07.Size = new System.Drawing.Size(96, 17);
            this.chxCB07.TabIndex = 11;
            this.chxCB07.Text = "(07) Set Offline";
            this.chxCB07.UseVisualStyleBackColor = true;
            // 
            // chxCB16
            // 
            this.chxCB16.AutoSize = true;
            this.chxCB16.Location = new System.Drawing.Point(21, 131);
            this.chxCB16.Name = "chxCB16";
            this.chxCB16.Size = new System.Drawing.Size(114, 17);
            this.chxCB16.TabIndex = 12;
            this.chxCB16.Text = "(16) Set User Data";
            this.chxCB16.UseVisualStyleBackColor = true;
            // 
            // chxCB24
            // 
            this.chxCB24.AutoSize = true;
            this.chxCB24.Location = new System.Drawing.Point(21, 159);
            this.chxCB24.Name = "chxCB24";
            this.chxCB24.Size = new System.Drawing.Size(96, 17);
            this.chxCB24.TabIndex = 13;
            this.chxCB24.Text = "(24) Align Req.";
            this.chxCB24.UseVisualStyleBackColor = true;
            // 
            // chxSB14
            // 
            this.chxSB14.AutoSize = true;
            this.chxSB14.Location = new System.Drawing.Point(246, 247);
            this.chxSB14.Name = "chxSB14";
            this.chxSB14.Size = new System.Drawing.Size(127, 17);
            this.chxSB14.TabIndex = 18;
            this.chxSB14.Text = "(14) Load Recipe Fail";
            this.chxSB14.UseVisualStyleBackColor = true;
            // 
            // chxSB09
            // 
            this.chxSB09.AutoSize = true;
            this.chxSB09.BackColor = System.Drawing.SystemColors.Control;
            this.chxSB09.Location = new System.Drawing.Point(246, 131);
            this.chxSB09.Name = "chxSB09";
            this.chxSB09.Size = new System.Drawing.Size(115, 17);
            this.chxSB09.TabIndex = 17;
            this.chxSB09.Text = "(9) Cmd Completed";
            this.chxSB09.UseVisualStyleBackColor = false;
            // 
            // chxSB08
            // 
            this.chxSB08.AutoSize = true;
            this.chxSB08.Location = new System.Drawing.Point(246, 103);
            this.chxSB08.Name = "chxSB08";
            this.chxSB08.Size = new System.Drawing.Size(112, 17);
            this.chxSB08.TabIndex = 16;
            this.chxSB08.Text = "(8) Cmd Executing";
            this.chxSB08.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(246, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Status Block";
            // 
            // chxSB07
            // 
            this.chxSB07.AutoSize = true;
            this.chxSB07.Location = new System.Drawing.Point(246, 75);
            this.chxSB07.Name = "chxSB07";
            this.chxSB07.Size = new System.Drawing.Size(77, 17);
            this.chxSB07.TabIndex = 14;
            this.chxSB07.Text = "(07) Online";
            this.chxSB07.UseVisualStyleBackColor = true;
            // 
            // chxSB24
            // 
            this.chxSB24.AutoSize = true;
            this.chxSB24.Location = new System.Drawing.Point(246, 304);
            this.chxSB24.Name = "chxSB24";
            this.chxSB24.Size = new System.Drawing.Size(118, 17);
            this.chxSB24.TabIndex = 20;
            this.chxSB24.Text = "(24) Align Req. Ack";
            this.chxSB24.UseVisualStyleBackColor = true;
            // 
            // chxSB16
            // 
            this.chxSB16.AutoSize = true;
            this.chxSB16.Location = new System.Drawing.Point(246, 276);
            this.chxSB16.Name = "chxSB16";
            this.chxSB16.Size = new System.Drawing.Size(136, 17);
            this.chxSB16.TabIndex = 19;
            this.chxSB16.Text = "(16) Set User Data Ack";
            this.chxSB16.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 185);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "Address";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 352);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 22;
            this.label4.Text = "Address";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(153, 185);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 13);
            this.label5.TabIndex = 23;
            this.label5.Text = "Value";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(153, 352);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 13);
            this.label6.TabIndex = 24;
            this.label6.Text = "Value";
            // 
            // txtWrAddress1
            // 
            this.txtWrAddress1.Location = new System.Drawing.Point(67, 222);
            this.txtWrAddress1.Name = "txtWrAddress1";
            this.txtWrAddress1.Size = new System.Drawing.Size(77, 20);
            this.txtWrAddress1.TabIndex = 27;
            this.txtWrAddress1.Text = "D8004";
            // 
            // txtWrValue1
            // 
            this.txtWrValue1.Location = new System.Drawing.Point(190, 222);
            this.txtWrValue1.Name = "txtWrValue1";
            this.txtWrValue1.Size = new System.Drawing.Size(153, 20);
            this.txtWrValue1.TabIndex = 26;
            // 
            // txtWrAddress2
            // 
            this.txtWrAddress2.BackColor = System.Drawing.SystemColors.Info;
            this.txtWrAddress2.Location = new System.Drawing.Point(67, 263);
            this.txtWrAddress2.Name = "txtWrAddress2";
            this.txtWrAddress2.Size = new System.Drawing.Size(77, 20);
            this.txtWrAddress2.TabIndex = 29;
            this.txtWrAddress2.Text = "D8012";
            // 
            // txtWrValue2
            // 
            this.txtWrValue2.BackColor = System.Drawing.SystemColors.Info;
            this.txtWrValue2.Location = new System.Drawing.Point(190, 263);
            this.txtWrValue2.Name = "txtWrValue2";
            this.txtWrValue2.Size = new System.Drawing.Size(153, 20);
            this.txtWrValue2.TabIndex = 28;
            // 
            // txtIBAddress
            // 
            this.txtIBAddress.Location = new System.Drawing.Point(11, 38);
            this.txtIBAddress.Name = "txtIBAddress";
            this.txtIBAddress.Size = new System.Drawing.Size(77, 20);
            this.txtIBAddress.TabIndex = 30;
            this.txtIBAddress.Text = "D8002";
            // 
            // txtOBAddress
            // 
            this.txtOBAddress.Location = new System.Drawing.Point(11, 25);
            this.txtOBAddress.Name = "txtOBAddress";
            this.txtOBAddress.Size = new System.Drawing.Size(77, 20);
            this.txtOBAddress.TabIndex = 55;
            this.txtOBAddress.Text = "D8107";
            // 
            // txtIBValue
            // 
            this.txtIBValue.Location = new System.Drawing.Point(140, 38);
            this.txtIBValue.Name = "txtIBValue";
            this.txtIBValue.Size = new System.Drawing.Size(661, 20);
            this.txtIBValue.TabIndex = 74;
            // 
            // txtOBValue
            // 
            this.txtOBValue.Location = new System.Drawing.Point(140, 25);
            this.txtOBValue.Name = "txtOBValue";
            this.txtOBValue.Size = new System.Drawing.Size(661, 20);
            this.txtOBValue.TabIndex = 75;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(103, 41);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(34, 13);
            this.label27.TabIndex = 76;
            this.label27.Text = "Value";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(103, 30);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(34, 13);
            this.label28.TabIndex = 77;
            this.label28.Text = "Value";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(3, 327);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(34, 13);
            this.label29.TabIndex = 79;
            this.label29.Text = "Value";
            // 
            // txtCBValue
            // 
            this.txtCBValue.Location = new System.Drawing.Point(5, 346);
            this.txtCBValue.Multiline = true;
            this.txtCBValue.Name = "txtCBValue";
            this.txtCBValue.Size = new System.Drawing.Size(210, 24);
            this.txtCBValue.TabIndex = 78;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(232, 327);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(34, 13);
            this.label30.TabIndex = 81;
            this.label30.Text = "Value";
            // 
            // txtSBValue
            // 
            this.txtSBValue.Location = new System.Drawing.Point(234, 346);
            this.txtSBValue.Multiline = true;
            this.txtSBValue.Name = "txtSBValue";
            this.txtSBValue.Size = new System.Drawing.Size(210, 24);
            this.txtSBValue.TabIndex = 80;
            // 
            // txtCBAddress
            // 
            this.txtCBAddress.Location = new System.Drawing.Point(90, 34);
            this.txtCBAddress.Name = "txtCBAddress";
            this.txtCBAddress.Size = new System.Drawing.Size(77, 20);
            this.txtCBAddress.TabIndex = 82;
            this.txtCBAddress.Text = "D8000";
            // 
            // txtSBAddress
            // 
            this.txtSBAddress.Location = new System.Drawing.Point(314, 33);
            this.txtSBAddress.Name = "txtSBAddress";
            this.txtSBAddress.Size = new System.Drawing.Size(77, 20);
            this.txtSBAddress.TabIndex = 83;
            this.txtSBAddress.Text = "D8100";
            // 
            // txtWrAddress3
            // 
            this.txtWrAddress3.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.txtWrAddress3.Location = new System.Drawing.Point(67, 304);
            this.txtWrAddress3.Name = "txtWrAddress3";
            this.txtWrAddress3.Size = new System.Drawing.Size(77, 20);
            this.txtWrAddress3.TabIndex = 85;
            this.txtWrAddress3.Text = "D8018";
            // 
            // txtWrValue3
            // 
            this.txtWrValue3.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.txtWrValue3.Location = new System.Drawing.Point(190, 304);
            this.txtWrValue3.Name = "txtWrValue3";
            this.txtWrValue3.Size = new System.Drawing.Size(153, 20);
            this.txtWrValue3.TabIndex = 84;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(153, 269);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(30, 13);
            this.label31.TabIndex = 86;
            this.label31.Text = "Float";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(153, 308);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(34, 13);
            this.label32.TabIndex = 87;
            this.label32.Text = "String";
            // 
            // txtIPAddress
            // 
            this.txtIPAddress.Location = new System.Drawing.Point(9, 44);
            this.txtIPAddress.Name = "txtIPAddress";
            this.txtIPAddress.Size = new System.Drawing.Size(136, 20);
            this.txtIPAddress.TabIndex = 88;
            this.txtIPAddress.Text = "192.168.10.39";
            // 
            // txtPort
            // 
            this.txtPort.Location = new System.Drawing.Point(155, 46);
            this.txtPort.Name = "txtPort";
            this.txtPort.Size = new System.Drawing.Size(65, 20);
            this.txtPort.TabIndex = 89;
            this.txtPort.Text = "3005";
            // 
            // txtIBValueFloat
            // 
            this.txtIBValueFloat.Location = new System.Drawing.Point(140, 82);
            this.txtIBValueFloat.Name = "txtIBValueFloat";
            this.txtIBValueFloat.Size = new System.Drawing.Size(661, 20);
            this.txtIBValueFloat.TabIndex = 90;
            // 
            // txtOBValueFloat
            // 
            this.txtOBValueFloat.Location = new System.Drawing.Point(140, 76);
            this.txtOBValueFloat.Name = "txtOBValueFloat";
            this.txtOBValueFloat.Size = new System.Drawing.Size(661, 20);
            this.txtOBValueFloat.TabIndex = 91;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(104, 86);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(30, 13);
            this.label8.TabIndex = 92;
            this.label8.Text = "Float";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(103, 79);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(30, 13);
            this.label9.TabIndex = 93;
            this.label9.Text = "Float";
            // 
            // txtOBValueTrayBit
            // 
            this.txtOBValueTrayBit.Location = new System.Drawing.Point(140, 117);
            this.txtOBValueTrayBit.Multiline = true;
            this.txtOBValueTrayBit.Name = "txtOBValueTrayBit";
            this.txtOBValueTrayBit.Size = new System.Drawing.Size(660, 22);
            this.txtOBValueTrayBit.TabIndex = 94;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(103, 122);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(28, 13);
            this.label10.TabIndex = 95;
            this.label10.Text = "Tray";
            // 
            // txtIBValueBarcode
            // 
            this.txtIBValueBarcode.Location = new System.Drawing.Point(141, 122);
            this.txtIBValueBarcode.Name = "txtIBValueBarcode";
            this.txtIBValueBarcode.Size = new System.Drawing.Size(661, 20);
            this.txtIBValueBarcode.TabIndex = 96;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(93, 127);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(47, 13);
            this.label11.TabIndex = 97;
            this.label11.Text = "Barcode";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(153, 143);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(34, 13);
            this.label12.TabIndex = 102;
            this.label12.Text = "Value";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(17, 143);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(45, 13);
            this.label13.TabIndex = 101;
            this.label13.Text = "Address";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(279, 138);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(64, 25);
            this.button3.TabIndex = 100;
            this.button3.Text = "Write";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // txtWrAddress
            // 
            this.txtWrAddress.Location = new System.Drawing.Point(67, 140);
            this.txtWrAddress.Name = "txtWrAddress";
            this.txtWrAddress.Size = new System.Drawing.Size(77, 20);
            this.txtWrAddress.TabIndex = 99;
            this.txtWrAddress.Text = "D8000";
            // 
            // txtWrValue
            // 
            this.txtWrValue.Location = new System.Drawing.Point(190, 140);
            this.txtWrValue.Name = "txtWrValue";
            this.txtWrValue.Size = new System.Drawing.Size(77, 20);
            this.txtWrValue.TabIndex = 98;
            this.txtWrValue.Text = "1";
            // 
            // txtIBAddressFloat
            // 
            this.txtIBAddressFloat.Location = new System.Drawing.Point(11, 80);
            this.txtIBAddressFloat.Name = "txtIBAddressFloat";
            this.txtIBAddressFloat.Size = new System.Drawing.Size(77, 20);
            this.txtIBAddressFloat.TabIndex = 103;
            this.txtIBAddressFloat.Text = "D8010";
            // 
            // txtIBAddressBarcode
            // 
            this.txtIBAddressBarcode.Location = new System.Drawing.Point(11, 121);
            this.txtIBAddressBarcode.Name = "txtIBAddressBarcode";
            this.txtIBAddressBarcode.Size = new System.Drawing.Size(77, 20);
            this.txtIBAddressBarcode.TabIndex = 104;
            this.txtIBAddressBarcode.Text = "D8016";
            // 
            // txtOBAddressFloat
            // 
            this.txtOBAddressFloat.Location = new System.Drawing.Point(12, 73);
            this.txtOBAddressFloat.Name = "txtOBAddressFloat";
            this.txtOBAddressFloat.Size = new System.Drawing.Size(77, 20);
            this.txtOBAddressFloat.TabIndex = 105;
            this.txtOBAddressFloat.Text = "D8116";
            // 
            // txtOBAddressTray
            // 
            this.txtOBAddressTray.Location = new System.Drawing.Point(12, 117);
            this.txtOBAddressTray.Name = "txtOBAddressTray";
            this.txtOBAddressTray.Size = new System.Drawing.Size(77, 20);
            this.txtOBAddressTray.TabIndex = 106;
            this.txtOBAddressTray.Text = "D8142";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(299, 85);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(45, 25);
            this.button4.TabIndex = 107;
            this.button4.Text = "Stop";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Visible = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(249, 85);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(45, 25);
            this.button5.TabIndex = 108;
            this.button5.Text = "Start";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Visible = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(170, 91);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(76, 13);
            this.lblStatus.TabIndex = 109;
            this.lblStatus.Text = "Disconnected!";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtIBAddressFloat);
            this.groupBox1.Controls.Add(this.txtIBAddress);
            this.groupBox1.Controls.Add(this.txtIBAddressBarcode);
            this.groupBox1.Controls.Add(this.txtIBValue);
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.txtIBValueFloat);
            this.groupBox1.Controls.Add(this.txtIBValueBarcode);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Location = new System.Drawing.Point(10, 406);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(819, 170);
            this.groupBox1.TabIndex = 110;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Input Block Address";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.checkBox1);
            this.groupBox2.Controls.Add(this.checkBox2);
            this.groupBox2.Controls.Add(this.txtOBAddressTray);
            this.groupBox2.Controls.Add(this.txtOBValue);
            this.groupBox2.Controls.Add(this.txtOBAddressFloat);
            this.groupBox2.Controls.Add(this.txtOBAddress);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label28);
            this.groupBox2.Controls.Add(this.txtOBValueTrayBit);
            this.groupBox2.Controls.Add(this.txtOBValueFloat);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Location = new System.Drawing.Point(10, 586);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(819, 150);
            this.groupBox2.TabIndex = 111;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Output Block Address";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.BackColor = System.Drawing.SystemColors.Control;
            this.checkBox1.Location = new System.Drawing.Point(264, 53);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(115, 17);
            this.checkBox1.TabIndex = 115;
            this.checkBox1.Text = "(9) Cmd Completed";
            this.checkBox1.UseVisualStyleBackColor = false;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(140, 54);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(112, 17);
            this.checkBox2.TabIndex = 115;
            this.checkBox2.Text = "(8) Cmd Executing";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.chxSB13);
            this.groupBox3.Controls.Add(this.chxSB12);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.lblTaktTime);
            this.groupBox3.Controls.Add(this.chxSB09);
            this.groupBox3.Controls.Add(this.chxSB11);
            this.groupBox3.Controls.Add(this.txtCBValue);
            this.groupBox3.Controls.Add(this.label29);
            this.groupBox3.Controls.Add(this.txtSBValue);
            this.groupBox3.Controls.Add(this.label30);
            this.groupBox3.Controls.Add(this.chxSB07);
            this.groupBox3.Controls.Add(this.chxCB04);
            this.groupBox3.Controls.Add(this.chxCB07);
            this.groupBox3.Controls.Add(this.chxCB16);
            this.groupBox3.Controls.Add(this.chxCB24);
            this.groupBox3.Controls.Add(this.chxSB08);
            this.groupBox3.Controls.Add(this.chxSB14);
            this.groupBox3.Controls.Add(this.chxSB16);
            this.groupBox3.Controls.Add(this.chxSB24);
            this.groupBox3.Controls.Add(this.txtCBAddress);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.txtSBAddress);
            this.groupBox3.Location = new System.Drawing.Point(375, 13);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(454, 379);
            this.groupBox3.TabIndex = 112;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Monitor Status";
            // 
            // chxSB13
            // 
            this.chxSB13.AutoSize = true;
            this.chxSB13.Location = new System.Drawing.Point(246, 218);
            this.chxSB13.Name = "chxSB13";
            this.chxSB13.Size = new System.Drawing.Size(161, 17);
            this.chxSB13.TabIndex = 118;
            this.chxSB13.Text = "(13) Load Recipe Completed";
            this.chxSB13.UseVisualStyleBackColor = true;
            // 
            // chxSB12
            // 
            this.chxSB12.AutoSize = true;
            this.chxSB12.Location = new System.Drawing.Point(246, 188);
            this.chxSB12.Name = "chxSB12";
            this.chxSB12.Size = new System.Drawing.Size(122, 17);
            this.chxSB12.TabIndex = 117;
            this.chxSB12.Text = "(12) Loading Recipe";
            this.chxSB12.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(375, 132);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 13);
            this.label7.TabIndex = 116;
            this.label7.Text = "0.0 ms";
            // 
            // lblTaktTime
            // 
            this.lblTaktTime.AutoSize = true;
            this.lblTaktTime.Location = new System.Drawing.Point(375, 159);
            this.lblTaktTime.Name = "lblTaktTime";
            this.lblTaktTime.Size = new System.Drawing.Size(38, 13);
            this.lblTaktTime.TabIndex = 110;
            this.lblTaktTime.Text = "0.0 ms";
            // 
            // chxSB11
            // 
            this.chxSB11.AutoSize = true;
            this.chxSB11.BackColor = System.Drawing.SystemColors.Control;
            this.chxSB11.Location = new System.Drawing.Point(246, 159);
            this.chxSB11.Name = "chxSB11";
            this.chxSB11.Size = new System.Drawing.Size(100, 17);
            this.chxSB11.TabIndex = 114;
            this.chxSB11.Text = "(11) Align Finish";
            this.chxSB11.UseVisualStyleBackColor = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.button6);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this.txtCycleTime);
            this.groupBox4.Controls.Add(this.lblStatus);
            this.groupBox4.Controls.Add(this.txtWrAddress0);
            this.groupBox4.Controls.Add(this.button5);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.button4);
            this.groupBox4.Controls.Add(this.txtWrValue);
            this.groupBox4.Controls.Add(this.txtWrAddress);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this.button2);
            this.groupBox4.Controls.Add(this.button3);
            this.groupBox4.Controls.Add(this.button1);
            this.groupBox4.Controls.Add(this.txtWrValue0);
            this.groupBox4.Controls.Add(this.btnWrite);
            this.groupBox4.Controls.Add(this.txtValueRead);
            this.groupBox4.Controls.Add(this.txtReadAddress);
            this.groupBox4.Controls.Add(this.btnRead);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this.txtPort);
            this.groupBox4.Controls.Add(this.txtWrValue1);
            this.groupBox4.Controls.Add(this.txtIPAddress);
            this.groupBox4.Controls.Add(this.txtWrAddress1);
            this.groupBox4.Controls.Add(this.label32);
            this.groupBox4.Controls.Add(this.txtWrValue2);
            this.groupBox4.Controls.Add(this.label31);
            this.groupBox4.Controls.Add(this.txtWrAddress2);
            this.groupBox4.Controls.Add(this.txtWrAddress3);
            this.groupBox4.Controls.Add(this.txtWrValue3);
            this.groupBox4.Location = new System.Drawing.Point(10, 13);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(360, 379);
            this.groupBox4.TabIndex = 113;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Connection";
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(9, 219);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(51, 25);
            this.button6.TabIndex = 114;
            this.button6.Text = "Clear";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(243, 25);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(106, 13);
            this.label16.TabIndex = 113;
            this.label16.Text = "Auto Cycle Time (ms)";
            this.label16.Visible = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(153, 25);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(49, 13);
            this.label15.TabIndex = 112;
            this.label15.Text = "PLC Port";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(10, 25);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(80, 13);
            this.label14.TabIndex = 111;
            this.label14.Text = "PLC IP address";
            // 
            // txtCycleTime
            // 
            this.txtCycleTime.Location = new System.Drawing.Point(249, 46);
            this.txtCycleTime.Name = "txtCycleTime";
            this.txtCycleTime.Size = new System.Drawing.Size(65, 20);
            this.txtCycleTime.TabIndex = 110;
            this.txtCycleTime.Text = "10000";
            this.txtCycleTime.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(836, 744);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PLC Register Monitor - Release 10012020";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox txtWrValue0;
        private System.Windows.Forms.TextBox txtWrAddress0;
        private System.Windows.Forms.Button btnWrite;
        private System.Windows.Forms.Button btnRead;
        private System.Windows.Forms.TextBox txtReadAddress;
        private System.Windows.Forms.TextBox txtValueRead;
        private System.Windows.Forms.CheckBox chxCB04;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chxCB07;
        private System.Windows.Forms.CheckBox chxCB16;
        private System.Windows.Forms.CheckBox chxCB24;
        private System.Windows.Forms.CheckBox chxSB14;
        private System.Windows.Forms.CheckBox chxSB09;
        private System.Windows.Forms.CheckBox chxSB08;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox chxSB07;
        private System.Windows.Forms.CheckBox chxSB24;
        private System.Windows.Forms.CheckBox chxSB16;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtWrAddress1;
        private System.Windows.Forms.TextBox txtWrValue1;
        private System.Windows.Forms.TextBox txtWrAddress2;
        private System.Windows.Forms.TextBox txtWrValue2;
        private System.Windows.Forms.TextBox txtIBAddress;
        private System.Windows.Forms.TextBox txtOBAddress;
        private System.Windows.Forms.TextBox txtIBValue;
        private System.Windows.Forms.TextBox txtOBValue;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txtCBValue;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox txtSBValue;
        private System.Windows.Forms.TextBox txtCBAddress;
        private System.Windows.Forms.TextBox txtSBAddress;
        private System.Windows.Forms.TextBox txtWrAddress3;
        private System.Windows.Forms.TextBox txtWrValue3;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox txtIPAddress;
        private System.Windows.Forms.TextBox txtPort;
        private System.Windows.Forms.TextBox txtIBValueFloat;
        private System.Windows.Forms.TextBox txtOBValueFloat;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtOBValueTrayBit;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtIBValueBarcode;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox txtWrAddress;
        private System.Windows.Forms.TextBox txtWrValue;
        private System.Windows.Forms.TextBox txtIBAddressFloat;
        private System.Windows.Forms.TextBox txtIBAddressBarcode;
        private System.Windows.Forms.TextBox txtOBAddressFloat;
        private System.Windows.Forms.TextBox txtOBAddressTray;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.CheckBox chxSB11;
        private System.Windows.Forms.Label lblTaktTime;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtCycleTime;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.CheckBox chxSB13;
        private System.Windows.Forms.CheckBox chxSB12;
    }
}

